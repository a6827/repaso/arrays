- Declaración e inicialización de un array de enteros de tamaño 10

- Introducción de valores en el array y visualización de los mismos: en cada posición el contenido será su número de posición

- Visualización del número de elementos distintos de cero que hay en el array.